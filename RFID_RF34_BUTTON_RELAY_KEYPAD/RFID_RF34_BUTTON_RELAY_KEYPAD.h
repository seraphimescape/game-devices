
///////////////////////////////
//  header starts here
//////////////////////////////
#ifndef DEVICES_H     
#define DEVICES_H

#define DEV_DEBUG

// temporary to enable the correct device ids and peripherals
//#define MASTER
#define SLAVE

// Define Devices for this sketch
#define RADIO
#ifdef SLAVE
   //#define RFID
   //#define BUTTON
   //#define KEY_PAD
   //#define BUZZER
   #define MAG_SWITCH
   //#define RELAY
#endif

#ifdef MASTER
   #define MY_DEVICE_ID 255      //id of this device.
   #define HUB_DEVICE_ID 20      //hub id
   //#define SERIAL_COMMS
#else
   #define MY_DEVICE_ID 20         //id of this device.
   #define HUB_DEVICE_ID 255       //hub id
#endif 


#ifdef KEY_PAD
  #include <Key.h>        // keypad
  #include <Keypad.h>     // keypad
#endif
#include <MemoryFree.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include "RF24.h"         // transceiver 
//#include <printf.h>
#ifdef RFID
  #include <MFRC522.h>    // RF ID reader
#endif

#ifdef DEV_DEBUG
  #define PRINT Serial.print
  #define PRINTLN Serial.println
#else
  #define PRINT //
  #define PRINTLN //
#endif

#define BAUD_RATE 115200
#define RADIO_CHANNEL 3

#define RFID_SS_PIN 8           //RFID cable select
#define RF24_SS_PIN 9           //RF24 cable select
#define RST_PIN 10              //shared reset

/////////////////////////////
// button pins 0 1 2 3 4 5
/////////////////////////////
#define BTN_PIN0 A0              //button pin
#define BTN_PIN1 A1
#define BTN_PIN2 A2
#define BTN_PIN3 A3
#define BTN_PIN4 A4
#define BTN_PIN5 A5

///////////////////////////////
// relay pins  4 5 6 7 8
//////////////////////////////
#define REL_PIN1 4              // relay pin
#define REL_PIN2 5
#define REL_PIN3 6
#define REL_PIN4 7
#define REL_PIN5 8

////////////////////////////
// keypad
///////////////////////////
#define LEFT_PIN1 A0              // Left pin on keyp pad pin1
#define LEFT_PIN2 A1
#define LEFT_PIN3 A2
#define LEFT_PIN4 A3
#define LEFT_PIN5 A4
#define LEFT_PIN6 A5
#define LEFT_PIN7 4
#define LEFT_PIN8 5

#ifdef KEY_PAD
  uint8_t keypad_digits[10];
  uint8_t digit_count = 0;
  // configure the keypad
  const uint8_t KEYPAD_ROWS = 4;
  const uint8_t KEYPAD_COLS = 4;
  char keypadKeys[KEYPAD_ROWS][KEYPAD_COLS] = {
    {'1','2','3','A'},
    {'4','5','6','B'},
    {'7','8','9','C'},
    {'*','0','#','D'} 
  };
  uint8_t KEYPAD_ROW_PINS[] = {LEFT_PIN1,LEFT_PIN2,LEFT_PIN3,LEFT_PIN4};
  uint8_t KEYPAD_COL_PINS[] = {LEFT_PIN5,LEFT_PIN6,LEFT_PIN7,LEFT_PIN8};
#endif

/////////////////////////////////
// BUZZER 
/////////////////////////////////
#define BUZZER_PIN 6

#ifdef BUZZER 
const uint16_t BUZZER_SOUNDS[][4] = {
                              {1000, 100,500, 100}   // Hz and duration, repeat
                              ,{2000, 500,1000, 200}   // Hz and duration, repeat
};

#endif


///////////////////////////////////
// Magnetic Switch
///////////////////////////////////

#ifdef MAG_SWITCH
bool SWITCH_STATE[] = {false,false,false,false,false,false,false};
/*
bool SWITCH_STATE_0 = false;
bool SWITCH_STATE_1 = false;
bool SWITCH_STATE_2 = false;
bool SWITCH_STATE_3 = false;
bool SWITCH_STATE_4 = false;
bool SWITCH_STATE_5 = false;
bool SWITCH_STATE_6 = false;
*/
#endif

/////////////////////////////////////////////
// Radio Message data structures
// NOTE: addding a new enum
//  1.  Add to enum here
//  2.  Update ParseJson and PacketToJs
//  3.  Approriate ActionTypeInt,etc. Method
//  4.  Add to String array here.
/////////////////////////////////////////////
enum ActionType
{
   noneAT = 0,
   laser,
   relay,
   buzzer,
   play
};
const char*  ActionTypeString[] = {"noneAT","laser","relay","buzzer","play"};

enum Action
{
   noneA = 0,
   start,
   stop
};
const char* ActionString[] = {"noneA","start","stop"};

enum Event
{
   noneE = 0,
   toggle,
   code,
   on,
   off
};
const char* EventString[] = {"noneE","toggle","code","on","off"};

enum EventType
{
   noneET = 0,
   button,
   keyPad,
   rfId,
   photoRes,
   thermometer,
   potentiometer,
   magnetSwitch
};
const char* EventTypeString[] = {"noneET","button","keyPad","rfId","photoRes","thermometer","potentiometer", "magnetSwitch"};

struct Packet
{
  uint8_t msgId = 0;
  uint8_t fromId = 0;
  uint8_t toId = 0;
  Action action;
  ActionType actionType;
  Event event;
  EventType eventType;
  uint8_t data[16];
};

struct Message
{
  long lastSentTime = 0;
  uint8_t sentTimes = 0;
  Packet packet;
};


#endif
