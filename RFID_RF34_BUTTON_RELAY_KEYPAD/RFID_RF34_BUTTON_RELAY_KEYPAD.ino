#include "RFID_RF34_BUTTON_RELAY_KEYPAD.h"

// Hardware configuration
// Set up nRF24L01 radio on SPI bus plus pins 7 & 8
RF24 radio(RST_PIN, RF24_SS_PIN);             // create the radio instance
#ifdef RFID
  MFRC522 mfrc522(RFID_SS_PIN, RST_PIN);        // Create the RFID MFRC522 instance.
#endif

#ifdef KEY_PAD
  Keypad keypad = Keypad(makeKeymap(keypadKeys), KEYPAD_ROW_PINS, KEYPAD_COL_PINS, KEYPAD_ROWS, KEYPAD_COLS);
#endif
                                        
// Use the same address for both devices
uint8_t address[] = { "radio" };

//debounce timer
unsigned long lastDebounceTime=millis();
unsigned long btnLastDebounceTime=millis();

//for testing count packets sent to received
long pSent = 0;
long pReceived = 0;

Packet receivedPacket;
StaticJsonBuffer<150> jsonBuffer;

/********************** Setup *********************/
void setup(){

  // Setup and configure rf radio
  radio.begin();
  //radio.setChannel(10);
  radio.setAutoAck(true);
  radio.setRetries(15,15);
  radio.enableDynamicPayloads();
  radio.openWritingPipe(address);             // communicate back and forth.  One listens on it, the other talks to it.
  radio.openReadingPipe(1,address); 
  radio.setDataRate(RF24_250KBPS );
  radio.txDelay = 4000;
  radio.startListening();

  Serial.begin(BAUD_RATE);
  PRINT("My Device ID: ");
  PRINTLN(MY_DEVICE_ID); 
  PRINT("PA Level: ");
  PRINTLN(radio.getPALevel());
  PRINT("Packet Size: ");
  PRINTLN(sizeof(Packet));
  PRINT("Channel #: ");
  PRINTLN(radio.getChannel());
  
    
  //radio.printDetails();                      // Dump the configuration of the rf unit for debugging
  attachInterrupt(0, check_radio, FALLING);        // Attach interrupt handler to interrupt #0 (using pin 2) on BOTH the sender and receiver

  #ifdef RFID
  // configure RFID Reader
     delay(500);
     PRINTLN("Using Card Reader Events");
     mfrc522.PCD_Init(); // Init MFRC522 card 
  #endif

  //configure button
  #ifdef BUTTON
     PRINTLN("Using Button Events");
     pinMode(BTN_PIN0, INPUT_PULLUP);
     pinMode(BTN_PIN1, INPUT_PULLUP);
     pinMode(BTN_PIN2, INPUT_PULLUP);
     pinMode(BTN_PIN3, INPUT_PULLUP);
     pinMode(BTN_PIN4, INPUT_PULLUP);
     pinMode(BTN_PIN5, INPUT_PULLUP);
  #endif

  // configure magnetic switch
  #ifdef MAG_SWITCH
     PRINTLN("Using Mag Switch Events");
     pinMode(BTN_PIN0, INPUT_PULLUP);
     pinMode(BTN_PIN1, INPUT_PULLUP);
     pinMode(BTN_PIN2, INPUT_PULLUP);
     pinMode(BTN_PIN3, INPUT_PULLUP);
     pinMode(BTN_PIN4, INPUT_PULLUP);
     pinMode(BTN_PIN5, INPUT_PULLUP);
  #endif 
  
  //configure relay
  #ifdef RELAY
     PRINTLN("Using Relay Requests");
     pinMode(REL_PIN1, INPUT_PULLUP);
     pinMode(REL_PIN1, OUTPUT);
  #endif

  // configure key pad
  #ifdef KEY_PAD
      PRINTLN("Using Keypad Events");
      memset(keypad_digits, 0, sizeof(keypad_digits));
  #endif

  #ifdef BUZZER
      PRINTLN("Using Buzzer");
      pinMode(BUZZER_PIN, OUTPUT);
  #endif

  #ifdef SERIAL_COMMS
     PRINTLN("Using Serial Port for Json Requests and Events");
  #endif

  // setup the packet data
  memset(&receivedPacket, 0, sizeof(Packet));
}



/********************** Main Loop *********************/
void loop() {
//    PRINT("free memory: ");
//    PRINTLN(getFreeMemory());  
    delay(100);

    // send the packet prepared by one of the Processes
    if (receivedPacket.toId != 0 && receivedPacket.toId != MY_DEVICE_ID)
    {
          PRINT("Sending ToId: ");
          PRINTLN(receivedPacket.toId);
          WriteData(&receivedPacket);
          delay(100);
          memset(&receivedPacket, 0, sizeof(Packet));
          receivedPacket.toId = 0;
          #ifdef RFID 
           mfrc522.PCD_Init();    // Init MFRC522 card  
          #endif 
    }
    else if (receivedPacket.toId == MY_DEVICE_ID)
    {
         PRINT("Received fromId: ");
         PRINTLN(receivedPacket.fromId);
         if (receivedPacket.actionType != noneAT)
         {
              PRINTLN("Processing Action Request");
              ProcessPacket(&receivedPacket);
         }      
         receivedPacket.toId = 0;
          memset(&receivedPacket, 0, sizeof(Packet));
         
    }
    /////////////////////////////
    // Process card reader Event
    /////////////////////////////
    #ifdef RFID
       ProcessCardReader();
    #endif

    ///////////////////////
    // Process button event
    ///////////////////////
    #ifdef BUTTON
       ButtonEvent();
    #endif

    ///////////////////////////////
    // Process Key Pad Events
    ///////////////////////////////
    #ifdef KEY_PAD
       ProcessKeyPad();
    #endif

    #ifdef BUZZER
       
    #endif

    #ifdef MAG_SWITCH
       MagneticSwtichEvent();
    #endif
    
    ///////////////////////
    // Process serial incomming data
    ///////////////////////
    #ifdef SERIAL_COMMS
         SerialReadEvent();
    #endif

}

void WriteData(Packet *packet)
{
    radio.stopListening();
    // Normal delay will not work here, so cycle through some no-operations (16nops @16mhz = 1us delay)
/*    for(uint32_t i=0; i<130;i++){
       __asm__("nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t");
    }
*/
    radio.startWrite((uint8_t*) packet,sizeof(Packet),0);
}

/*************************************************************** 
 * Serial Read Event from Pi
 ***************************************************************/
#ifdef SERIAL_COMMS
  char inString[150];
  void SerialReadEvent(){
    int inStringLen = 0;
    inString[0] = '\0';
  
    // if the packet already has something in it, bail.
    if (receivedPacket.toId > 0) return;
     
    if (Serial.available()) PRINTLN("Serial data is available.");
    
    // Serial is USB and Serial1 is pins 0 or 1.
    while (Serial.available()) {
      // get the new byte:
      char inChar = (char)Serial.read();
      
      // add it to the inputString:
      inString[inStringLen] = inChar;
      inStringLen++;
      delay(10);
    }
    inString[inStringLen] = '\0';
  
    if (inStringLen == 0) return;
  
   // output the string recieved
    PRINT("Serial Received: ");
    PRINT(inStringLen);
    PRINTLN(" Bytes");
    PRINTLN("===================");
    PRINTLN(inString);
    PRINTLN("===================");
  
    receivedPacket = ParseJson(inString);
  }
#endif

#ifdef MAG_SWITCH
  void MagneticSwtichEvent()
  {
    if (receivedPacket.toId > 0) return;
  
    int button0 = digitalRead(BTN_PIN0);
    int button1 = digitalRead(BTN_PIN1);
    int button2 = digitalRead(BTN_PIN2);
    int button3 = digitalRead(BTN_PIN3);
    int button4 = digitalRead(BTN_PIN4);
    int button5 = digitalRead(BTN_PIN5);
    int switchNo = -1;

    if (button0 == LOW && !SWITCH_STATE[0] || button0 == HIGH && SWITCH_STATE[0])  switchNo = 0;
    else if(button1 == LOW && !SWITCH_STATE[1] || button1 == HIGH && SWITCH_STATE[1])  switchNo = 1;
    else if(button2 == LOW && !SWITCH_STATE[2] || button1 == HIGH && SWITCH_STATE[2])  switchNo = 2;
    else if(button3 == LOW && !SWITCH_STATE[3] || button1 == HIGH && SWITCH_STATE[3])  switchNo = 3;
    else if(button4 == LOW && !SWITCH_STATE[4] || button1 == HIGH && SWITCH_STATE[4])  switchNo = 4;
    else if(button5 == LOW && !SWITCH_STATE[5] || button1 == HIGH && SWITCH_STATE[5])  switchNo = 5;
      
    if (switchNo >= 0)
    {
         PRINTLN("Magnet Switch Event: ");
         
        SWITCH_STATE[switchNo] = !SWITCH_STATE[switchNo];
        //prepare packet for button
        receivedPacket.fromId = MY_DEVICE_ID;
        receivedPacket.toId = HUB_DEVICE_ID;
        receivedPacket.eventType = magnetSwitch;
        if (SWITCH_STATE[switchNo]) receivedPacket.event = on;
        else receivedPacket.event = off;
        memcpy((void*) receivedPacket.data, (void*)&switchNo, sizeof(switchNo));
    }
  }
#endif

#ifdef KEY_PAD
  void ProcessKeyPad()
  { 
      char key = keypad.getKey();
  
      if (key) {
        keypad_digits[digit_count] = key;
        PRINT("Key Pad digit buffer: ");
        PRINTLN((char*) keypad_digits);
        digit_count++;
      
        if (digit_count == sizeof(keypad_digits)) 
        {
           // package code and Key Pad event
          receivedPacket.fromId = MY_DEVICE_ID;
          receivedPacket.toId = HUB_DEVICE_ID;
          receivedPacket.event = code;
          receivedPacket.eventType = keyPad;      
          memcpy((void*) receivedPacket.data, (void*)keypad_digits, sizeof(keypad_digits));
          // clear the pad
          digit_count = 0;
          memset(keypad_digits, 0, sizeof(keypad_digits));
  
          #ifdef BUZZER
             tone(BUZZER_PIN, 1000);
             delay(100);
             noTone(BUZZER_PIN);
          #endif
        }
        else 
        {
          #ifdef BUZZER
            tone(BUZZER_PIN, 500);
            delay(100);
            noTone(BUZZER_PIN);
          #endif
        }
      }
  }
#endif

/*************************************************************** 
 * Button Event code
 ***************************************************************/
#ifdef BUTTON
void ButtonEvent(){

    // if the packet already has something in it, bail.
    if (receivedPacket.toId > 0) return;
  
    int button0 = digitalRead(BTN_PIN0);
    int button1 = digitalRead(BTN_PIN1);
    int button2 = digitalRead(BTN_PIN2);
    int button3 = digitalRead(BTN_PIN3);
    int button4 = digitalRead(BTN_PIN4);
    int button5 = digitalRead(BTN_PIN5);
    int buttonNo = 0;

    
    if (button0 == LOW || button1 == LOW || button2 == LOW || button3 == LOW || button4 == LOW || button5 == LOW){
      if ((millis()-btnLastDebounceTime) < 125)  //if 50 milliseconds has passed since last bounce
      {
           btnLastDebounceTime=millis();
           return;  //read value again now that bouncing is over
      }    
      btnLastDebounceTime=millis();
  
      PRINTLN("Button Event: ");

      //prepare packet for button
      receivedPacket.fromId = MY_DEVICE_ID;
      receivedPacket.toId = HUB_DEVICE_ID;
      receivedPacket.event = toggle;
      receivedPacket.eventType = button;
  
      //prepare message to be sent
      if(button0 == LOW) buttonNo = 0;
      if(button1 == LOW) buttonNo = 1;
      if(button2 == LOW) buttonNo = 2;
      if(button3 == LOW) buttonNo = 3;
      if(button4 == LOW) buttonNo = 4;
      if(button5 == LOW) buttonNo = 5;
     
      memcpy((void*) receivedPacket.data, (void*)&buttonNo, sizeof(buttonNo));
    }  
}
#endif

/*************************************************************** 
 * RFID Reader Event code
 ***************************************************************/
#ifdef RFID
  void FormatCardID(uint8_t *buffer, byte* uidByte, byte bufferSize) {
      for (byte i = 0; i < bufferSize; i++) {
          sprintf((char*) buffer+(i*2), "%0X", uidByte[i]);
      }
  }

  void ProcessCardReader()
  {
      // if the packet already has something in it, bail.
      if (receivedPacket.toId > 0) return;
    
      if ( ! mfrc522.PICC_IsNewCardPresent())
      {
          return;
      }
      // Select one of the cards
      if ( ! mfrc522.PICC_ReadCardSerial())
      {
          PRINTLN("Not Read Card Searial");   
          return;
      }
  
      if ((millis()-lastDebounceTime) < 300)  //if 300 milliseconds has passed since last bounce
      {  
           lastDebounceTime=millis();
           return;  //read value again now that bouncing is over
      }    
      lastDebounceTime=millis();
  
      PRINTLN("Card Reader Event: ");
      
      // send card Id via radio here
      receivedPacket.fromId = MY_DEVICE_ID;
      receivedPacket.toId = HUB_DEVICE_ID;
      receivedPacket.event = code;
      receivedPacket.eventType = rfId;
      FormatCardID((uint8_t*) receivedPacket.data, mfrc522.uid.uidByte, mfrc522.uid.size);
  }
#endif

/*************************************************************************
 * Radio Processing Events and Request Code here
 *************************************************************************/
void ProcessRelayRequest(Packet *packet)
{
  #ifdef RELAY
   // received info from Master Arduino.
    PRINTLN("Relay Request: ");
    
    // &packet->data has the pin to switch
   uint8_t pinNo = *(unsigned int*) packet->data;
   // packet->action = either start or stop
   // temp - turn on relay 1.
   if (packet->action == stop) {
      PRINTLN("On");
      digitalWrite(pinNo, HIGH); 
   } else if (packet->action == start) {
      PRINTLN("Off");
      digitalWrite(pinNo, LOW); 
   }     
  #endif
}


void ProcessBuzzerRequest(Packet *packet)
{
#ifdef BUZZER  
    PRINTLN("Buzzer Request: ");

    tone(6,700);
    delay(200);
    noTone(6);
    
    uint8_t soundNo = *(unsigned int*) packet->data;
    PRINT("Request No: ");
    PRINTLN(soundNo);
    
    if (soundNo > 0 && soundNo <= 2)
    {    
        PRINTLN("Playing Buzzer");
        const uint16_t *melody = BUZZER_SOUNDS[soundNo-1];
        for (int i = 0; i < 4; i+=2)
        {
          PRINT(melody[i]);
          PRINT(", ");
          PRINTLN(melody[i+1]);
           tone(BUZZER_PIN, melody[i]);
           delay(melody[i+1]);
        }
        noTone(BUZZER_PIN);
        PRINTLN("Done");
    }
#endif 
}

void ProcessCardReaderEvent(Packet *packet)
{
   // send info to Raspberry Pi here.
    PRINTLN("Card Reader Tagged: ");

}

void ProcessButtonEvent(Packet *packet)
{
   // send info to Raspberry Pi here.
    PRINT("Button toggled: ");
 
}

void ProcessKeyPadEvent(Packet *packet)
{
   // send info to Raspberry Pi here.
    PRINT("Key Pad Code: ");
}

void ProcessPacket(Packet *packet)
{
    if (packet->toId == 0) return;
    PRINTLN("ParsePacket()");      
     
    if (packet->toId != MY_DEVICE_ID) 
    {
      PRINTLN("packet not for me");      
      return;
    }

    if (packet->action != noneA)
    {
       switch (packet->actionType)
       {
         case laser:
         break;
         case relay:
            ProcessRelayRequest(packet);
         break;
         case buzzer:
            ProcessBuzzerRequest(packet);
         break;
         case play:
         break;
         default:
         break;
       }
       memset(packet, 0, sizeof(Packet));
    }

    if (packet->event != noneE)
    {
       switch (packet->eventType)
       {
         case button:
            ProcessButtonEvent(packet);
         break;
         case keyPad:
            ProcessKeyPadEvent(packet);
         break;
         case rfId:
            ProcessCardReaderEvent(packet);
         break;
         case photoRes:
         break;
         case thermometer:
         break;
         case potentiometer:
         break;
         case magnetSwitch:
         break;
         default:
         break;
       }
       memset(packet, 0, sizeof(Packet));
    }    
    
}

/********************** Radio signal Interrupt *********************/
void check_radio(void)                                // Receiver role: Does nothing!  All the work is in IRQ
{
 // PRINTLN("check_radio entered");
 /*     for(uint32_t i=0; i<500;i++){
       __asm__("nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t");
    }
*/
  bool tx,fail,rx;
  radio.whatHappened(tx,fail,rx);                     // What happened?

 
  // If data is available, handle it accordingly
  if ( rx ){
    pReceived += 1;
    if(radio.getDynamicPayloadSize() < 1){
      PRINTLN("Corrupt payload has been flushed");
      return; 
    }
    
    // Read in the data
    PRINTLN("Read in the data");
    radio.read((uint8_t*)&receivedPacket,sizeof(Packet));

    // Print the packet just recieved.
    PRINTLN("Received Packet: ");
    PRINTLN("===================");
    JsonObject &root = PacketToJson(&receivedPacket);
    root.printTo(Serial);
    Serial.println();
    
    PRINTLN("===================");
    PRINT("Sent packets: ");
    PRINTLN(pSent);
    PRINT("Received packets");
    PRINTLN(pReceived);
    jsonBuffer.clear();
                
    // Process the resquest
    //ProcessPacket(&receivedPacket);
    
   // receivedPacket.toId = 0;
    //PRINT("Processed Packet from: ");
   // PRINT(receivedPacket.fromId);
   // PRINTLN(". Now clearing it");
    //memset(&receivedPacket, 0, sizeof(Packet));
   // PRINTLN("===================");
    //JsonObject &root2 = PacketToJson(&receivedPacket);
    //root2.prettyPrintTo(Serial);
    //PRINTLN("===================");
    //jsonBuffer.clear();
    PRINT("free memory: ");
    PRINTLN(getFreeMemory());
     
  }

  // Start listening if transmission is complete
  if( tx || fail ){
    radio.startListening();
    pSent += 1;
     if (tx) {
        // serial out json string
        PRINTLN("Sending Packet");
        PRINTLN("===================");
        JsonObject &root = PacketToJson(&receivedPacket);
        root.prettyPrintTo(Serial);
        PRINTLN("===================");
        jsonBuffer.clear();
        PRINT("Sent packets: ");
        PRINTLN(pSent);
        PRINT("Received packets");
        PRINTLN(pReceived);
     }
     else PRINTLN("Send:Fail");

  }  
}

JsonObject& PacketToJson(Packet *packet)
{
    JsonObject &root = jsonBuffer.createObject();
    root["fromId"] = packet->fromId;
    root["toId"] = packet->toId;
    root["action"] = ActionString[packet->action];
    root["actionType"] = ActionTypeString[packet->actionType];
    root["event"] = EventString[packet->event];
    root["eventType"] = EventTypeString[packet->eventType];
    if (packet->eventType == button || packet->actionType == relay 
        || packet->actionType == buzzer || packet->eventType == magnetSwitch) root["data"] = *(unsigned int*) packet->data;  
    else root["data"] = (char*) packet->data;  
    return root;
}

Packet ParseJson(char* jsonString)
{
    Packet packet;
    
    JsonObject &root = jsonBuffer.parseObject(jsonString);
    packet.fromId = root["fromId"];
    packet.toId = root["toId"] ;
    packet.action = ActionInt((char*)root.get<const char*>("action"));
    packet.actionType =  ActionTypeInt((char*)root.get<const char*>("actionType"));
    packet.event = EventInt((char*)root.get<const char*>("event"));
    packet.eventType = EventTypeInt((char*)root.get<const char*>("eventType"));
    if (packet.eventType == button || packet.actionType == relay || packet.actionType == buzzer || packet.eventType == magnetSwitch) {
      int a = root.get<unsigned int>("data");
      PRINT("uint value: ");      
      PRINTLN(a);      
      //memset(receivedPacket.data, 0, 22);
      memcpy((void*) packet.data, (void*) &a, sizeof(int));  
      PRINTLN("uint value assigned to packet.data");
    }
    else memcpy((void*) packet.data, (void*) root.get<const char*>("data"), 22);
    
    return packet;
}

ActionType ActionTypeInt(char* actionType)
{
   if (strcasecmp(actionType, ActionTypeString[0]) == 0) return noneAT;
   if (strcasecmp(actionType, ActionTypeString[1]) == 0) return laser;
   if (strcasecmp(actionType, ActionTypeString[2]) == 0) return relay;
   if (strcasecmp(actionType, ActionTypeString[3]) == 0) return buzzer;
   if (strcasecmp(actionType, ActionTypeString[4]) == 0) return play;
   return noneAT;
}

Action ActionInt(char* action)
{
   if (strcasecmp(action, ActionString[0]) == 0) return noneA;
   if (strcasecmp(action, ActionString[1]) == 0) return start;
   if (strcasecmp(action, ActionString[2]) == 0) return stop;
   return noneA;
}

Event EventInt(char* event)
{
   if (strcasecmp(event, EventString[0]) == 0) return noneE;
   if (strcasecmp(event, EventString[1]) == 0) return toggle;
   if (strcasecmp(event, EventString[2]) == 0) return code;
   if (strcasecmp(event, EventString[3]) == 0) return on;
   if (strcasecmp(event, EventString[4]) == 0) return off;
   return noneE;
}
EventType EventTypeInt(char *eventType)
{
   if (strcasecmp(eventType, EventTypeString[0]) == 0) return noneET;
   if (strcasecmp(eventType, EventTypeString[1]) == 0) return button;
   if (strcasecmp(eventType, EventTypeString[2]) == 0) return keyPad;
   if (strcasecmp(eventType, EventTypeString[3]) == 0) return rfId;
   if (strcasecmp(eventType, EventTypeString[4]) == 0) return photoRes;
   if (strcasecmp(eventType, EventTypeString[5]) == 0) return thermometer;
   if (strcasecmp(eventType, EventTypeString[6]) == 0) return potentiometer;
   if (strcasecmp(eventType, EventTypeString[7]) == 0) return magnetSwitch;
   return  noneET;
}



